package com.tims.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerInfoRequest {
	
	//@JsonProperty("CUSTOMER_ID")
	//private String customerId;
	
	@JsonProperty("HASHED_VIN")
	private String hashedVin;
	
	@JsonProperty("FIRST_NM")
	private String firstName;
	
	@JsonProperty("LAST_NM")
	private String lastName;	
	
	@JsonProperty("MAIL_ADR_POSTAL_CD")
	private String zipCode;
	
	//public String getCustomerId() {
	//	return customerId;
	// }
	public String getHashedVin() {
		return hashedVin;
	}
	public void setHashedVin(String hashedVin) {
		this.hashedVin = hashedVin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
}
