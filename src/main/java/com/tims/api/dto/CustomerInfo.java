package com.tims.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerInfo {
   
	@JsonProperty("HASHED_VIN")
	private String hashedVin;
	
	@JsonProperty("CUSTOMER_ID")
	private String customerId;
	
	@JsonProperty("VIN")
	private String vin;
	
	@JsonProperty("FIRST_NM")
	private String firstName;
	
	@JsonProperty("MIDDLE_INITIAL")
	private String middleName;
	
	@JsonProperty("LAST_NM")
	private String lastName;
	
	@JsonProperty("ORGANIZATION_NM")
	private String organizationName;
	
	@JsonProperty("ADDRESS_LINE_1")
	private String mailAddressLine1;
	
	@JsonProperty("ADDRESS_LINE_2")
	private String mailAddressLine2;
	
	@JsonProperty("CITY_NM")
	private String mailAddressCity;
	
	@JsonProperty("STATE_CD")
	private String mailAddressState;
	
	@JsonProperty("ZIP")
	private String mailAddressPostal;
	
	@JsonProperty("COUNTRY_NM")
	private String mailAddressCountry;
	
	@JsonProperty("EMAIL_ADDRESS")
	private String emailAddress;
	
	@JsonProperty("CELL_PHONE")
	private String cellPhone;
	
	@JsonProperty("HOME_PHONE")
	private String homePhome;
	
	@JsonProperty("WORK_PHONE")
	private String workPhone;
	
	public String getHashedVin() {
		return hashedVin;
	}
	public void setHashedVin(String hashedVin) {
		this.hashedVin = hashedVin;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getVin() {
		return vin;
	}
	public void setVin(String vin) {
		this.vin = vin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getOrganizationName() {
		return organizationName;
	}
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}
	public String getMailAddressLine1() {
		return mailAddressLine1;
	}
	public void setMailAddressLine1(String mailAddressLine1) {
		this.mailAddressLine1 = mailAddressLine1;
	}
	public String getMailAddressLine2() {
		return mailAddressLine2;
	}
	public void setMailAddressLine2(String mailAddressLine2) {
		this.mailAddressLine2 = mailAddressLine2;
	}
	public String getMailAddressCity() {
		return mailAddressCity;
	}
	public void setMailAddressCity(String mailAddressCity) {
		this.mailAddressCity = mailAddressCity;
	}
	public String getMailAddressState() {
		return mailAddressState;
	}
	public void setMailAddressState(String mailAddressState) {
		this.mailAddressState = mailAddressState;
	}
	public String getMailAddressPostal() {
		return mailAddressPostal;
	}
	public void setMailAddressPostal(String mailAddressPostal) {
		this.mailAddressPostal = mailAddressPostal;
	}
	public String getMailAddressCountry() {
		return mailAddressCountry;
	}
	public void setMailAddressCountry(String mailAddressCountry) {
		this.mailAddressCountry = mailAddressCountry;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public String getHomePhome() {
		return homePhome;
	}
	public void setHomePhome(String homePhome) {
		this.homePhome = homePhome;
	}
	public String getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}
	
	
   	
}
