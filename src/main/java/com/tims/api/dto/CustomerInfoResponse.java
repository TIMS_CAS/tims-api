package com.tims.api.dto;

public class CustomerInfoResponse {
	
	private CustomerInfo customerInfo;
	private String responsCode;

	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}

	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	
	public String getResponsCode() {
		return responsCode;
	}

	public void setResponsCode(String responsCode) {
		this.responsCode = responsCode;
	}

}
