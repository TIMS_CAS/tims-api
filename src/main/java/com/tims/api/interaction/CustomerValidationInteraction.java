package com.tims.api.interaction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tims.api.dto.CustomerInfo;
import com.tims.api.dto.CustomerInfoRequest;
import com.tims.api.dto.CustomerInfoResponse;
import com.tims.api.utility.SqlServerConnector;

@Service
public class CustomerValidationInteraction {

	@Autowired
	SqlServerConnector sqlServerConnector;
	private final String OPT_IN_OUT_STATUS = "I";

	private final String customerValidationQuery = "select * from  PII where HASHED_VIN=";

	public CustomerInfoResponse validateCustomerInfo(CustomerInfoRequest customerInfoRequest) {

		CustomerInfoResponse customerInfoResponse = new CustomerInfoResponse();
		String responseCode = "404"; // default response-code
		try {
			Statement statement = sqlServerConnector.getConnection().createStatement();
			ResultSet resultSet = statement
					.executeQuery(customerValidationQuery + "'" + customerInfoRequest.getHashedVin().trim() + "'");
			while (resultSet.next()) {
				if (customerInfoRequest.getLastName().toLowerCase().trim().equals(resultSet.getString("LAST_NM").toLowerCase())
						&& customerInfoRequest.getZipCode().trim()
								.equals(resultSet.getString("ZIP").substring(0, 5).trim())
						&& resultSet.getString("TIMS_PREF_IND").equals(OPT_IN_OUT_STATUS)) {
					responseCode = "200";
					System.out.println("Enter 1");
					CustomerInfo customerInfo = new CustomerInfo();
					// building the response object
					buildCustomerInfoResponse(customerInfo,resultSet);
					customerInfoResponse.setCustomerInfo(customerInfo);
				} 
			}
			System.out.println("Enter 2");
			customerInfoResponse.setResponsCode(responseCode);
			sqlServerConnector.getConnection().close();
		} catch (SQLException e) {
			System.out.println(e);
			e.printStackTrace();
			System.out.println("Enter 3");
		} catch (Exception e) {
			System.out.println("Enter 4");
		}
		return customerInfoResponse;
	}

	private void buildCustomerInfoResponse(CustomerInfo customerInfo,ResultSet resultSet) {
		try {
			customerInfo.setHashedVin(resultSet.getString("HASHED_VIN")!=null?resultSet.getString("HASHED_VIN"):"");
			customerInfo.setCustomerId(resultSet.getString("CUSTOMER_ID")!=null?resultSet.getString("CUSTOMER_ID"):"");
			customerInfo.setVin(resultSet.getString("VIN")!=null?resultSet.getString("VIN"):"");
			customerInfo.setFirstName(resultSet.getString("FIRST_NM")!=null?resultSet.getString("FIRST_NM"):"");
			customerInfo.setMiddleName(resultSet.getString("MIDDLE_INITIAL")!=null?resultSet.getString("MIDDLE_INITIAL"):"");
			customerInfo.setLastName(resultSet.getString("LAST_NM")!=null?resultSet.getString("LAST_NM"):"");
			customerInfo.setOrganizationName(resultSet.getString("ORGANIZATION_NM")!=null?resultSet.getString("ORGANIZATION_NM"):"");
			customerInfo.setMailAddressLine1(resultSet.getString("ADDRESS_LINE_1")!=null?resultSet.getString("ADDRESS_LINE_1"):"");
			customerInfo.setMailAddressLine2(resultSet.getString("ADDRESS_LINE_2")!=null?resultSet.getString("ADDRESS_LINE_2"):"");
			customerInfo.setMailAddressCity(resultSet.getString("CITY_NM")!=null?resultSet.getString("CITY_NM"):"");
			customerInfo.setMailAddressState(resultSet.getString("STATE_CD")!=null?resultSet.getString("STATE_CD"):"");
			customerInfo.setMailAddressPostal(resultSet.getString("ZIP")!=null?resultSet.getString("ZIP"):"");
			customerInfo.setMailAddressCountry(resultSet.getString("COUNTRY_NM")!=null?resultSet.getString("COUNTRY_NM"):"");
			customerInfo.setEmailAddress(resultSet.getString("EMAIL_ADDRESS")!=null?resultSet.getString("EMAIL_ADDRESS"):"");
			customerInfo.setCellPhone(resultSet.getString("CELL_PHONE")!=null?resultSet.getString("CELL_PHONE"):"");
			customerInfo.setHomePhome(resultSet.getString("HOME_PHONE")!=null?resultSet.getString("HOME_PHONE"):"");
			customerInfo.setWorkPhone(resultSet.getString("WORK_PHONE")!=null?resultSet.getString("WORK_PHONE"):"");
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
