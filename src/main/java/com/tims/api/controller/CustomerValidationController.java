package com.tims.api.controller;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tims.api.dto.CustomerInfoRequest;
import com.tims.api.dto.CustomerInfoResponse;
import com.tims.api.interaction.CustomerValidationInteraction;



@RestController
public class CustomerValidationController {
	
	@Autowired
	CustomerValidationInteraction customerValidationInteraction;
	

	@RequestMapping(value="/customervalidation",
			method= RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public CustomerInfoResponse validateData(@RequestBody CustomerInfoRequest customerInfoRequest){
		return customerValidationInteraction.validateCustomerInfo(customerInfoRequest);
	}

}
