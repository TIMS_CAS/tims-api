package com.tims.api.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

@Service
public class SqlServerConnector {
	
	static final String hostName = "apisqldevserver.database.windows.net";
	//static final String hostName = "apisqlprodserver.database.windows.net";
	static final String dbName = "valapidevsqldb";
	//static final String dbName = "valapiprodsqldb";
	static final String user = "sql-admin";
	static final String password = "G3t1nsur@nce";
	//static final String password = "G3t1nsur@nce?";
	static final String connectionUrl = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);
    Connection connection = null;
	
    public Connection getConnection(){
    	try {
			connection = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	return connection;
    }
}
