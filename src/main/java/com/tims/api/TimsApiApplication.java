package com.tims.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimsApiApplication.class, args);
	}
}
